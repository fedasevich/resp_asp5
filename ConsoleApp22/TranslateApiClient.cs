﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    using System.Net.Http.Json;
    using System.Net;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using Microsoft.Extensions.Configuration;

    public class ApiClient
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;
  

        public ApiClient(IConfigurationRoot configuration)
        {
            _configuration = configuration;
            _client = new HttpClient { BaseAddress = new Uri(_configuration["BaseAddress"]) };
            _client.DefaultRequestHeaders.Add("X-RapidAPI-Key", _configuration["ApiKey"]);
            _client.DefaultRequestHeaders.Add("X-RapidAPI-Host", _configuration["ApiHost"]);
        }

        public async Task<ApiResponse<RootResponse<List<LanguageListResult>>>> Get()
        {
            try
            {
                var response = await _client.GetAsync("getLanguages");
                var content = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<RootResponse<List<LanguageListResult>>>(content);

                if (data != null && data.Err != null)
                {
                    throw new HttpRequestException(data.Err, null, HttpStatusCode.InternalServerError);
                }

                return new ApiResponse<RootResponse<List<LanguageListResult>>>
                {
                    Message = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Data = data
                };
            }
            catch (Exception ex)
            {
                return new ApiResponse<RootResponse<List<LanguageListResult>>>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }

        public async Task<ApiResponse<TranslateResponse>> Post(TranslateRequest body)
        {
            try
            {
                var response = await _client.PostAsJsonAsync("translate",body);
                var content = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<TranslateResponse>(content);

                if (data != null && data.Err != null)
                {
                    throw new HttpRequestException(data.Err, null, HttpStatusCode.InternalServerError);
                }

                return new ApiResponse<TranslateResponse>
                {
                    Message = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Data = data
                };
            }
            catch (Exception ex)
            {
                return new ApiResponse<TranslateResponse>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError
                };
            }
        }
    }

    public class ApiResponse<T>
    {
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public T Data { get; set; }
    }


    public class RootResponse<T>
    {
        [JsonPropertyName("err")]
        public string Err { get; set; }

        [JsonPropertyName("result")]
        public T Result { get; set; }
    }


    public class LanguageListResult
    {
        [JsonPropertyName("full_code")]
        public string FullCode { get; set; }

        [JsonPropertyName("code_alpha_1")]
        public string CodeAlpha1 { get; set; }

        [JsonPropertyName("englishName")]
        public string EnglishName { get; set; }

        [JsonPropertyName("codeName")]
        public string CodeName { get; set; }

        [JsonPropertyName("flagPath")]
        public string FlagPath { get; set; }

        [JsonPropertyName("testWordForSyntezis")]
        public string TestWordForSyntezis { get; set; }

        [JsonPropertyName("rtl")]
        public string Rtl { get; set; }

        [JsonPropertyName("modes")]
        public List<Mode> Modes { get; set; }
    }

    public class Mode
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("value")]
        public bool Value { get; set; }
    }


    public class TranslateRequest
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Data { get; set; }
    }

    public class TranslateResponse : RootResponse<string>
    {
    }
}
