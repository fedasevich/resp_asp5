﻿using ConsoleApp22;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;

public class Program
{
    private static bool IsValidLanguageFormat(string language)
    {
        var pattern = @"^[a-z]{2}_[A-Z]{2}$";
        var regex = new Regex(pattern);
        return regex.IsMatch(language);
    }
    public static async Task Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        var builder = new ConfigurationBuilder();
        builder.AddUserSecrets<Program>();
        var config = builder.Build();

        var apiClient = new ApiClient(config);
        var languageListResponse = await apiClient.Get();
            
        if (languageListResponse.Data != null)
        {
            Console.WriteLine($"Status code: { languageListResponse.StatusCode}");
            Console.WriteLine($"Message: {languageListResponse.Message}");

            foreach (var result in languageListResponse.Data.Result)
            {
                Console.WriteLine($"Full Code: {result.FullCode}");
                Console.WriteLine($"Code Alpha 1: {result.CodeAlpha1}");
                Console.WriteLine($"English Name: {result.EnglishName}");
                Console.WriteLine($"Code Name: {result.CodeName}");
                Console.WriteLine($"Flag Path: {result.FlagPath}");
                Console.WriteLine($"Test Word For Syntezis: {result.TestWordForSyntezis}");
                Console.WriteLine($"Rtl: {result.Rtl}");

                foreach (var mode in result.Modes)
                {
                    Console.WriteLine($"Mode Name: {mode.Name}");
                    Console.WriteLine($"Mode Value: {mode.Value}");
                }

                Console.WriteLine();
            }
        }
        else
        {
            Console.WriteLine($"Error: {languageListResponse.Message}");
        }

        Console.Write("Enter the source language: ");
        string fromLanguage = Console.ReadLine();
        while (!IsValidLanguageFormat(fromLanguage))
        {
            Console.Write("Invalid format. Enter the source language again: ");
            fromLanguage = Console.ReadLine();
        }

        Console.Write("Enter the target language: ");
        string toLanguage = Console.ReadLine();
        while (!IsValidLanguageFormat(toLanguage))
        {
            Console.Write("Invalid format. Enter the target language again: ");
            toLanguage = Console.ReadLine();
        }

        Console.Write("Enter the text to translate: ");
        string data = Console.ReadLine();

        var translateRequest = new TranslateRequest
        {
            From = fromLanguage,
            To = toLanguage,
            Data = data
        };

        var translateResponse = await apiClient.Post(translateRequest);

        if (languageListResponse.Data != null)
        {
            Console.WriteLine($"Result: {translateResponse.Data.Result}");

        }
        else
        {
            Console.WriteLine($"Error: {translateResponse.Message}");
        }
    }
}